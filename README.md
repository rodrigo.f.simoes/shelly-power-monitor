# Shelly Power Monitor

# Deploy
- Docker (registry.gitlab.com/rodrigo.f.simoes/shelly-power-monitor:main)
- Set up environment variables (Use .env or Docker compose):
    - SHELLY_IP
- Expose ports (8080 is default)     
- API Available on `http://host:port/api/docs`


## Shelly Plus 2 PM
#### Expected schema
`curl -X POST -d '{"id":1,"method":"Shelly.GetStatus"}' http://${SHELLY}/rpc`
```
"id": 1,
  "src": "-redacted-",
  "result": {
    "ble": {},
    "cloud": {
      "connected": true
    },
    "input:0": {
      "id": 0,
      "state": false
    },
    "input:1": {
      "id": 1,
      "state": false
    },
    "mqtt": {
      "connected": false
    },
    "switch:0": {
      "id": 0,
      "source": "SHC",
      "output": false,
      "apower": 0,
      "voltage": 202.9,
      "freq": 50,
      "current": 0,
      "pf": 0,
      "aenergy": {
        "total": 0,
        "by_minute": [
          0,
          0,
          0
        ],
        "minute_ts": 1710780120
      },
      "ret_aenergy": {
        "total": 0,
        "by_minute": [
          0,
          0,
          0
        ],
        "minute_ts": 1710780120
      },
      "temperature": {
        "tC": 51.2,
        "tF": 124.1
      }
    },
    "switch:1": {
      "id": 1,
      "source": "SHC",
      "output": false,
      "apower": 0,
      "voltage": 202.9,
      "freq": 50,
      "current": 0,
      "pf": 0,
      "aenergy": {
        "total": 0,
        "by_minute": [
          0,
          0,
          0
        ],
        "minute_ts": 1710780120
      },
      "ret_aenergy": {
        "total": 0,
        "by_minute": [
          0,
          0,
          0
        ],
        "minute_ts": 1710780120
      },
      "temperature": {
        "tC": 51.2,
        "tF": 124.1
      }
    },
    "sys": {
      "mac": "-redacted-",
      "restart_required": false,
      "time": "16:42",
      "unixtime": 1710780147,
      "uptime": 3409,
      "ram_size": 258808,
      "ram_free": 111460,
      "fs_size": 458752,
      "fs_free": 131072,
      "cfg_rev": 13,
      "kvs_rev": 0,
      "schedule_rev": 0,
      "webhook_rev": 0,
      "available_updates": {},
      "reset_reason": 3
    },
    "wifi": {
      "sta_ip": "192.168.2.199",
      "status": "got ip",
      "ssid": "-redacted-",
      "rssi": -58
    },
    "ws": {
      "connected": false
    }
  }
}
```


(docker build -t registry.gitlab.com/rodrigo.f.simoes/shelly-power-monitor:main .) -and (docker push registry.gitlab.com/rodrigo.f.simoes/shelly-power-monitor:main)