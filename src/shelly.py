from statistics import StatisticsError, mean
import requests
from src.logging import get_logger
from src.schemas import GetStatus, Result
from settings import settings

logger = get_logger(__name__)


def get_shelly_data() -> GetStatus:
    response = requests.post(
        f"http://{settings.shelly_ip}/rpc", data='{"id":1,"method":"Shelly.GetStatus"}'
    )
    response.raise_for_status()
    return GetStatus.model_validate(response.json())


def supply_voltage(result: Result) -> float:
    switches = [
        switch
        for switch in [result.switch0, result.switch1, result.switch2, result.switch3]
        if switch
    ]
    try:
        mean(
            [
                switch.voltage
                for switch in filter(lambda switch: not switch.output, switches)
            ]
        )
    except StatisticsError:
        pass
    # Since more current draw creates more voltage drop, find the lower current consumer
    return min(switches, key=lambda switch: switch.current).voltage
