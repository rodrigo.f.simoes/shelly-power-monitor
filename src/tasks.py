from celery import Celery

from src.database import add_value
from src.shelly import get_shelly_data, supply_voltage

app = Celery("power_monitor", broker="redis://redis:6379/0")
app.conf.timezone = "UTC"


@app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    sender.add_periodic_task(1.0, save_voltage.s(), name="save voltage")


@app.task
def celery_hello():
    print("Hello, World!")


@app.task
def save_voltage():
    try:
        shelly_data = get_shelly_data()
        voltage = supply_voltage(shelly_data.result)
        unixtime = shelly_data.result.sys.unixtime
        add_value(timestamp=unixtime, voltage_value=voltage)
        return unixtime, voltage
    except Exception as expt:
        print(expt)
        pass
