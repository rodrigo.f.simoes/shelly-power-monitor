from datetime import datetime
from typing import Awaitable

import redis

from settings import settings

r = redis.Redis(host=settings.redis_host, port=settings.redis_port, decode_responses=True)
ts = r.ts()
r.ping()
try:
    ts.create("voltage_monitor")
except Exception as expt:
    print(expt)


def get_pools() -> list:
    return list(r.scan_iter())


def get_values() -> list[tuple[int, float]]:
    return ts.range("voltage_monitor", "-", "+")


def add_value(timestamp: int, voltage_value: float):
    return ts.add("voltage_monitor", timestamp, voltage_value, duplicate_policy="last")


def sample_number() -> int:
    return len(get_values())


def get_values_time_interval(start: datetime, end: datetime) -> list[tuple[int, float]]:
    return ts.range("voltage_monitor", int(start.timestamp()), int(end.timestamp()))
