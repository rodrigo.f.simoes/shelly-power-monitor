import io
from collections import defaultdict
from datetime import date, datetime, time, timedelta
from typing import Optional

import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import pandas as pd
import plotly.express as px
import plotly.graph_objects as go
from fastapi import APIRouter, BackgroundTasks, Response

from src.database import get_values, get_values_time_interval
from src.logging import get_logger

logger = get_logger(__name__)
images_api = APIRouter(prefix="/images")


@images_api.get("/day")
async def daily(background_tasks: BackgroundTasks, target_date: Optional[date] = None):
    target_date = target_date or date.today()
    start = datetime.combine(target_date, time())
    end = datetime.combine(target_date, time(hour=23, minute=59))

    data = get_values_time_interval(start, end)

    if not data:
        return Response(content="No data available for the selected date.", status_code=404)

    # Extracting timestamps and voltage values
    x = [datetime.fromtimestamp(entry[0]) for entry in data]
    y = [entry[1] for entry in data]

    # Setting up the figure
    plt.figure(figsize=(10, 6))

    plt.xlim(start, end)
    plt.grid(visible=True, which="both", linestyle="--", linewidth=0.5, alpha=0.7)

    # Title and Labels
    plt.title(f'Electrical Potential Difference Analysis\n{target_date.strftime("%d/%m/%Y")}', fontsize=14)
    plt.xlabel("Time (HH:MM)", fontsize=12)
    plt.ylabel("Voltage (V)", fontsize=12)

    # Add threshold lines with inline labels
    plt.axhline(y=230 * 1.06, color="blue", linestyle="--")
    plt.text(end, 230 * 1.06, "  230V + 6%", color="blue", va="center", fontsize=10)
    plt.axhline(y=230 * 0.9, color="red", linestyle="--")
    plt.text(end, 230 * 0.9, "  230V - 10%", color="red", va="center", fontsize=10)

    # Plot voltage data
    plt.plot(x, y, label="Electrical Potential Difference")

    # Format x-axis
    plt.gca().xaxis.set_major_formatter(mdates.DateFormatter("%H:%M"))
    plt.gcf().autofmt_xdate()
    plt.minorticks_on()

    # Save the plot to a buffer
    img_buf = io.BytesIO()
    plt.savefig(img_buf, format="png", bbox_inches="tight")
    plt.close()

    # Prepare response
    buf_contents = img_buf.getvalue()
    background_tasks.add_task(img_buf.close)

    headers = {
        "Content-Disposition": f'inline; filename="{target_date.strftime("%d_%m_%Y")}.png"'
    }

    return Response(buf_contents, headers=headers, media_type="image/png")


@images_api.get("/week")
async def weekly(background_tasks: BackgroundTasks, target_date: Optional[date] = None):
    target_date = target_date or date.today()
    start_of_week = target_date - timedelta(days=target_date.weekday() + 1)
    end_of_week = start_of_week + timedelta(days=6)

    daily_stats = []
    for i in range(7):
        day = start_of_week + timedelta(days=i)
        start = datetime.combine(day, time())
        end = datetime.combine(day, time(hour=23, minute=59))

        data = get_values_time_interval(start, end)

        if data:
            y = [entry[1] for entry in data]
            daily_stats.append((day, min(y), sum(y) / len(y), max(y)))
        else:
            daily_stats.append((day, None, None, None))

    days = [entry[0] for entry in daily_stats]
    mins = [entry[1] for entry in daily_stats]
    means = [entry[2] for entry in daily_stats]
    maxs = [entry[3] for entry in daily_stats]

    # Setting up the figure
    plt.figure(figsize=(10, 6))

    plt.grid(visible=True, which="both", linestyle="--", linewidth=0.5, alpha=0.7)

    # Title and Labels
    plt.title(f"Weekly Voltage Statistics ({start_of_week.strftime("%d_%m_%Y")})", fontsize=14)
    plt.xlabel("Day of Week (Day of Month)", fontsize=12)
    plt.ylabel("Voltage (V)", fontsize=12)

    # Plot min, mean, max values
    plt.plot(days, mins, label="Min Voltage", marker="o", color="red")
    plt.plot(days, means, label="Mean Voltage", marker="o", color="green")
    plt.plot(days, maxs, label="Max Voltage", marker="o", color="blue")

    # Add legend
    plt.legend(loc="upper right", fontsize=10, framealpha=0.9)

    # Format x-axis
    plt.gca().xaxis.set_major_formatter(mdates.DateFormatter("%a (%d)"))
    plt.gcf().autofmt_xdate()
    plt.minorticks_on()

    # Save the plot to a buffer
    img_buf = io.BytesIO()
    plt.savefig(img_buf, format="png", bbox_inches="tight")
    plt.close()

    # Prepare response
    buf_contents = img_buf.getvalue()
    background_tasks.add_task(img_buf.close)

    headers = {
        "Content-Disposition": f'inline; filename="weekly_statistics_{start_of_week.strftime("%d_%m_%Y")}.png"'
    }

    return Response(buf_contents, headers=headers, media_type="image/png")


@images_api.get("/month/hourly")
async def monthly_hourly_comparison(background_tasks: BackgroundTasks, target_date: Optional[date] = None):
    target_date = target_date or date.today()
    start_of_month = target_date.replace(day=1)
    end_of_month = (start_of_month + timedelta(days=31)).replace(day=1) - timedelta(days=1)

    hourly_stats = {hour: [] for hour in range(24)}

    for day in range(1, end_of_month.day + 1):
        current_date = start_of_month.replace(day=day)
        start = datetime.combine(current_date, time())
        end = datetime.combine(current_date, time(hour=23, minute=59))

        data = get_values_time_interval(start, end)

        for entry in data:
            entry_hour = datetime.fromtimestamp(entry[0]).hour
            hourly_stats[entry_hour].append(entry[1])

    hours = range(24)
    mins = [min(hourly_stats[hour]) if hourly_stats[hour] else None for hour in hours]
    means = [sum(hourly_stats[hour]) / len(hourly_stats[hour]) if hourly_stats[hour] else None for hour in hours]
    maxs = [max(hourly_stats[hour]) if hourly_stats[hour] else None for hour in hours]

    # Setting up the figure
    plt.figure(figsize=(10, 6))

    plt.xlim(0, 23)
    plt.xticks(ticks=range(24), labels=[f"{h:02}:00" for h in range(24)], rotation=45)
    plt.minorticks_on()
    plt.grid(visible=True, which="both", linestyle="--", linewidth=0.5, alpha=0.7)

    # Title and Labels
    plt.title(f"Monthly Hourly Voltage Comparison ({start_of_month.strftime("%B_%Y")})", fontsize=14)
    plt.xlabel("Hour of Day", fontsize=12)
    plt.ylabel("Voltage (V)", fontsize=12)

    # Add threshold lines with inline labels
    plt.axhline(y=230 * 1.06, color="blue", linestyle="--")
    plt.text(24, 230 * 1.06, "  230V + 6%", color="blue", va="center", fontsize=10)
    plt.axhline(y=230 * 0.9, color="red", linestyle="--")
    plt.text(24, 230 * 0.9, "  230V - 10%", color="red", va="center", fontsize=10)

    # Plot min, mean, max values
    plt.plot(hours, mins, label="Min Voltage", marker="o", color="red")
    plt.plot(hours, means, label="Mean Voltage", marker="o", color="green")
    plt.plot(hours, maxs, label="Max Voltage", marker="o", color="blue")

    # Add legend
    plt.legend(loc="upper right", fontsize=10, framealpha=0.9)

    # Save the plot to a buffer
    img_buf = io.BytesIO()
    plt.savefig(img_buf, format="png", bbox_inches="tight")
    plt.close()

    # Prepare response
    buf_contents = img_buf.getvalue()
    background_tasks.add_task(img_buf.close)

    headers = {
        "Content-Disposition": f'inline; filename="monthly_hourly_comparison_{start_of_month.strftime("%B_%Y")}.png"'
    }

    return Response(buf_contents, headers=headers, media_type="image/png")
@images_api.get("/analysis")
def analysis():
    values = get_values()
    df = pd.DataFrame([dict(datetime=datetime.fromtimestamp(value[0]), voltage=value[1]) for value in values])
    fig = px.line(
        df, x="datetime", y="voltage", title="Electrical Potential Difference"
    )
    fig.add_hline(
        y=230 * 1.06,
        line_dash="dot",
        annotation_text="230V + 6%",
        annotation_position="bottom right",
    )
    fig.add_hline(
        y=230 * 0.9,
        line_dash="dot",
        annotation_text="230V - 10%",
        annotation_position="bottom right",
    )
    fig.add_hrect(y0=230 * 1.06, y1=260, line_width=0, fillcolor="red", opacity=0.2)
    fig.add_hrect(y0=160, y1=230 * 0.9, line_width=0, fillcolor="red", opacity=0.2)
    return Response(fig.to_html())


@images_api.get("/percentage")
def percentage():
    fig = go.Figure()
    values = get_values()
    by_date = defaultdict(list)
    for entry in values:
        by_date[datetime.fromtimestamp(entry[0]).date().isoformat()].append(entry[1])

    y_data = [value for value in by_date.keys()]
    x_list = []
    for key, value in by_date.items():
        bellow = round(
            (len(list(filter(lambda this: this < (230 * 0.9), value))) / len(value))
            * 100,
            2,
        )
        good = round(
            (
                    len(
                        list(
                            filter(lambda this: (230 * 0.9) <= this <= (230 * 1.06), value)
                        )
                    )
                    / len(value)
            )
            * 100,
            2,
        )
        above = round(
            (len(list(filter(lambda this: this > (230 * 1.06), value))) / len(value))
            * 100,
            2,
        )
        x_list.append([bellow, good, above])

    colors = [
        "rgba(38, 24, 74, 0.8)",
        "rgba(71, 58, 131, 0.8)",
        "rgba(122, 120, 168, 0.8)",
    ]

    for i in range(0, len(x_list[0])):
        for xd, yd in zip(x_list, y_data):
            fig.add_trace(
                go.Bar(
                    x=[xd[i]],
                    y=[yd],
                    orientation="h",
                    marker=dict(
                        color=colors[i], line=dict(color="rgb(248, 248, 249)", width=1)
                    ),
                )
            )

    fig.update_layout(
        xaxis=dict(
            showgrid=False,
            showline=False,
            showticklabels=False,
            zeroline=False,
            domain=[0.15, 1],
        ),
        yaxis=dict(
            showgrid=False,
            showline=False,
            showticklabels=False,
            zeroline=False,
        ),
        barmode="stack",
        paper_bgcolor="rgb(248, 248, 255)",
        plot_bgcolor="rgb(248, 248, 255)",
        margin=dict(l=120, r=10, t=140, b=80),
        showlegend=False,
    )

    top_labels = ["Bellow", "Good", "Above"]

    annotations = []

    for yd, xd in zip(y_data, x_list):
        # labeling the y-axis
        annotations.append(
            dict(
                xref="paper",
                yref="y",
                x=0.14,
                y=yd,
                xanchor="right",
                text=str(yd),
                font=dict(family="Arial", size=14, color="rgb(67, 67, 67)"),
                showarrow=False,
                align="right",
            )
        )
        # labeling the first percentage of each bar (x_axis)
        annotations.append(
            dict(
                xref="x",
                yref="y",
                x=xd[0] / 2,
                y=yd,
                text=str(xd[0]) + "%",
                font=dict(family="Arial", size=14, color="rgb(248, 248, 255)"),
                showarrow=False,
            )
        )
        # labeling the first Likert scale (on the top)
        if yd == y_data[-1]:
            annotations.append(
                dict(
                    xref="x",
                    yref="paper",
                    x=xd[0] / 2,
                    y=1.1,
                    text=top_labels[0],
                    font=dict(family="Arial", size=14, color="rgb(67, 67, 67)"),
                    showarrow=False,
                )
            )
        space = xd[0]
        for i in range(1, len(xd)):
            # labeling the rest of percentages for each bar (x_axis)
            annotations.append(
                dict(
                    xref="x",
                    yref="y",
                    x=space + (xd[i] / 2),
                    y=yd,
                    text=str(xd[i]) + "%",
                    font=dict(family="Arial", size=14, color="rgb(248, 248, 255)"),
                    showarrow=False,
                )
            )
            # labeling the Likert scale
            if yd == y_data[-1]:
                annotations.append(
                    dict(
                        xref="x",
                        yref="paper",
                        x=space + (xd[i] / 2),
                        y=1.1,
                        text=top_labels[i],
                        font=dict(family="Arial", size=14, color="rgb(67, 67, 67)"),
                        showarrow=False,
                    )
                )
            space += xd[i]

    fig.update_layout(annotations=annotations)
    return Response(fig.to_html())
