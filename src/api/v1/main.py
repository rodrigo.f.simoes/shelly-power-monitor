from datetime import datetime, timedelta, timezone

from fastapi import APIRouter

from src.api.v1.images import images_api
from src.database import get_values, sample_number, get_values_time_interval
from src.logging import get_logger
from src.schemas import GetStatus
from src.shelly import get_shelly_data, supply_voltage
from src.tasks import celery_hello

logger = get_logger(__name__)
v1_api = APIRouter(prefix="/v1")

v1_api.include_router(images_api)


@v1_api.get("/healthy", status_code=302, response_model=str)
def healthy():
    return "Yes"


@v1_api.get("/shelly", response_model=GetStatus)
def get_status():
    return get_shelly_data()


@v1_api.get("/voltage", response_model=float)
def get_voltage():
    return supply_voltage(get_shelly_data().result)


@v1_api.get("/db/values")
def database_values():
    return get_values()


@v1_api.get("/db/values/datetime")
def database_values_time_interval(
        start: datetime = datetime.now(timezone.utc) - timedelta(hours=1),
        end: datetime = datetime.now(timezone.utc),
):
    return get_values_time_interval(start, end)


@v1_api.get("/db/values/count", response_model=int)
def database_values_count():
    return sample_number()


@v1_api.post("/test/celery")
def test_celery():
    celery_hello.delay()
    return "OK!"
