from fastapi import FastAPI, APIRouter
from fastapi.middleware.cors import CORSMiddleware
from starlette.middleware.sessions import SessionMiddleware
from starlette.responses import RedirectResponse

from src.api.v1.main import v1_api


app = FastAPI(
    title="Shelly Power Monitor", docs_url="/api/docs", openapi_url="/api/openapi.json"
)
app.add_middleware(SessionMiddleware, secret_key="secret-string")

origins = ["*"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/", include_in_schema=False)
async def test():
    return RedirectResponse("/api/docs")


api = APIRouter(prefix="/api")

api.include_router(v1_api)

app.include_router(api)
