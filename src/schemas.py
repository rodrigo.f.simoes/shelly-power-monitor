from datetime import datetime, timezone
from typing import Optional

from pydantic import BaseModel, Field, field_serializer, field_validator


class Switch(BaseModel):
    id: int
    # source: str
    output: bool
    # apower: int
    voltage: float
    # freq: int
    current: int
    # pf: int


class System(BaseModel):
    mac: str
    unixtime: int


class Result(BaseModel):
    switch0: Optional[Switch] = Field(None, alias="switch:0")
    switch1: Optional[Switch] = Field(None, alias="switch:1")
    switch2: Optional[Switch] = Field(None, alias="switch:2")
    switch3: Optional[Switch] = Field(None, alias="switch:3")
    sys: System


class GetStatus(BaseModel):
    id: int
    src: str
    result: Result


class DatabaseEntry(BaseModel):
    datetime: datetime
    voltage: float

    @field_validator("datetime")
    @classmethod
    def datetime_utc(cls, v) -> "datetime":
        return v.replace(tzinfo=timezone.utc)

    @field_serializer('datetime')
    def serialize_dt(self, dt: "datetime", _info):
        return dt.isoformat()
