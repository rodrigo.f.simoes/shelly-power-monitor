FROM python:3.12-slim

WORKDIR /code

RUN pip install uv

ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

COPY requirements.txt requirements.txt

RUN uv pip install --no-cache-dir --system -r requirements.txt

COPY . .

EXPOSE 8080