from typing import Union
from dotenv import load_dotenv
from pydantic import IPvAnyAddress
from pydantic_settings import BaseSettings

load_dotenv()


class Settings(BaseSettings):
    debug: bool = False

    api_host: str = "0.0.0.0"
    api_port: int = 8080

    redis_host: str = "redis"
    redis_port: int = 6379

    shelly_ip: Union[IPvAnyAddress, str]

    class Config:
        env_file = ".env"
        env_file_encoding = "utf-8"
        extra = "ignore"


settings = Settings()  # type: ignore
